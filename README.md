# Developers

Just a simple page that contains a Steve Ballmer "Developers" graphic
as ASCII-art.

Credits for the graphic goes to Oliver Geary. The original work can be
found at:

https://www.deviantart.com/olivergeary/art/Steve-Ballmer-Developers-t-shirt-331622357

